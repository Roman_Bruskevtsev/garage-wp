#!/bin/bash
mkdir -p build/config

rm -f build/wp-content/plugins/hello.php
rm -rf build/wp-content/themes/twenty*

cp  config/.htaccess build

cp  config/production-config.php build/config
cp  config/wp-config.php build
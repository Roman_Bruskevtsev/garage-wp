(function($) {
    $(window).on('load', function(){
        
        $('.loader__wrapper').addClass('hide__wrapper');
        
        /*Mobile menu*/
        $('.menu__btn').on('click', function(){
            $(this).toggleClass('show__menu');
            $('.mobile__menu').toggleClass('show__menu');
        });
        /*Main slider*/
        if($('.main__slider').length){
            $('.main__slider').slick({
                dots: true,
                arrows: false,
                autoplay: true,
                autoplaySpeed: 6000,
                infinite: true,
                speed: 500,
                slidesToShow: 1
            });
        }
        /*Gallery slider*/
        if($('.gallery__slider').length){
            $('.gallery__slider').slick({
                dots: false,
                arrows: true,
                fade: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1
            });
        }
        /*Benefints block*/
        $('.hover__block').hover(
            function(){
                $(this).addClass('hover');
                $('.hover__block').not(this).addClass('no__hover');
            },
            function(){
                $(this).removeClass('hover');
                $('.hover__block').not(this).removeClass('no__hover');
            }
        );
        AOS.init();
    });
    /*Google map*/
    $(function() {
        var marker = [], infowindow = [], map, image = $('.map-wrapper').attr('data-marker');

        function addMarker(location,name,contentstr){
            marker[name] = new google.maps.Marker({
                position: location,
                map: map,
                icon: image
            });
            marker[name].setMap(map);

            infowindow[name] = new google.maps.InfoWindow({
                content:contentstr
            });
            
            google.maps.event.addListener(marker[name], 'click', function() {
                infowindow[name].open(map,marker[name]);
            });
        }
        
        function initialize() {

            var lat = $('#map-canvas').attr("data-lat");
            var lng = $('#map-canvas').attr("data-lng");
            var mapStyle = $('#map-canvas').attr("data-style");

            var myLatlng = new google.maps.LatLng(lat,lng);

            var setZoom = parseInt($('#map-canvas').attr("data-zoom"));

            var styles = "";

            if (mapStyle=="1"){
                styles = [
                            {
                                "featureType": "administrative",
                                "elementType": "labels.text.fill",
                                "stylers": [
                                    {
                                        "color": "#444444"
                                    }
                                ]
                            },
                            {
                                "featureType": "landscape",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "color": "#f2f2f2"
                                    }
                                ]
                            },
                            {
                                "featureType": "poi",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "road",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "saturation": -100
                                    },
                                    {
                                        "lightness": 45
                                    }
                                ]
                            },
                            {
                                "featureType": "road.highway",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "simplified"
                                    }
                                ]
                            },
                            {
                                "featureType": "road.arterial",
                                "elementType": "labels.icon",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "transit",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "visibility": "off"
                                    }
                                ]
                            },
                            {
                                "featureType": "water",
                                "elementType": "all",
                                "stylers": [
                                    {
                                        "color": "#4f595d"
                                    },
                                    {
                                        "visibility": "on"
                                    }
                                ]
                            }
                        ];
            }
            var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});

            var mapOptions = {
                zoom: setZoom,
                disableDefaultUI: false,
                scrollwheel: false,
                zoomControl: true,
                streetViewControl: true,
                center: myLatlng
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
            
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
            

            $('.addresses-block a').each(function(){
                var mark_lat = $(this).attr('data-lat');
                var mark_lng = $(this).attr('data-lng');
                var this_index = $('.addresses-block a').index(this);
                var mark_name = 'template_marker_'+this_index;
                var mark_locat = new google.maps.LatLng(mark_lat, mark_lng);
                var mark_str = $(this).attr('data-string');
                addMarker(mark_locat,mark_name,mark_str);   
            });
            
        }

        
      if ($('.map-wrapper').length){    
        setTimeout(function(){
            initialize();
        }, 500);
      }
        
    });
})(jQuery);
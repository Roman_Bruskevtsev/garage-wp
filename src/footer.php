<?php
/**
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer__nav">
                        <div class="footer__logo">
                            <?php the_custom_logo(); ?>
                        </div>
                        <div class="two__line"></div>
                        <?php wp_nav_menu( array(
                            'theme_location'        => 'footer',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav'
                        ) ); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php if( get_field('copyright', 'option') ){ ?>
                    <div class="copyright">
                        <p><?php the_field('copyright', 'option'); ?></p>
                    </div>
                    <?php } ?>
                    <div class="social__links">
                        <?php if( get_field('facebook', 'option') ){ ?><a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="fb"></a><?php } ?>
                        <?php if( get_field('instagram', 'option') ){ ?><a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="in"></a><?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php wp_footer(); ?>

</body>
</html>
<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'content_editor' ):
            the_sub_field('text');
        elseif( get_row_layout() == 'full_height_slider' ): 
            get_template_part( 'inc/acf-content/full-height-slider' );
        elseif( get_row_layout() == 'posts_section' ): 
            get_template_part( 'inc/acf-content/posts-section' );
        elseif( get_row_layout() == 'benefits_section' ): 
            get_template_part( 'inc/acf-content/benefits-section' );
        elseif( get_row_layout() == 'content_section' ): 
            get_template_part( 'inc/acf-content/content-section' );
        endif;
    endwhile;
else :
    echo '
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="no__content">
                        <h1>'.__('Nothing to show', 'garage').'</h1>
                    </div>
                </div>
            </div>
        </div>
    ';
endif;
get_footer();
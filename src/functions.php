<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 */

/*Trainers post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Theme setup*/
function garage_setup() {
    load_theme_textdomain( 'garage' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-logo', array(
        'width'       => 163,
        'height'      => 127,
        'flex-width'  => true,
    ) );
    add_image_size( 'about-post-size', 422, 191, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'garage' ),
        'footer'        => __( 'Footer Menu', 'garage' )
    ) );
}
add_action( 'after_setup_theme', 'garage_setup' );

/*App2drive styles and scripts*/
function garage_scripts() {
    $version = '1.0';

    wp_enqueue_style( 'garage-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'garage-style', get_stylesheet_uri() );

    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=58e42895e8a8fc0011f81ed7&product=custom-share-buttons', array('jquery'), $version, true );
    wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assets/js/aos.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'garage_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Main Page Settings',
    //     'menu_title'    => 'Main Page',
    //     'parent_slug'   => $general['menu_slug']
    // ));
}


/*SVG support*/
function garage_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'garage_svg_types');

/*App2drive ajax*/
function garage_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}
add_action('wp_footer','garage_ajaxurl');

/*Time to read*/
function garage_reading_time() {

    $post = get_post();

    $words = '';
    $contents =  get_field_objects(get_the_ID());
    foreach ($contents as $content) {
        $fields_info = $content['value'];
        if(is_array($fields_info)){
            foreach ($fields_info as $field_info) {
                $text = $field_info["content_editor"];
                if($text) $words .= $text;
            }
        }
    }
    $words = strip_tags($words);
    $words = count(preg_split('/\s+/', $words));
    
    $minutes = floor( $words / 120 );
    $seconds = floor( $words % 120 / ( 120 / 60 ) );

    if ( 1 <= $minutes ) {
        if ( 1 == $minutes){
            $estimated_time = $minutes . MINUTE . ', ' . $seconds . SECONDS . FORREADING;
        } else {
            $estimated_time = $minutes . MINUTES . ', ' . $seconds . SECONDS . FORREADING;
        }
        
    } else {
        $estimated_time = $seconds . SECONDS . FORREADING;
    }

    return $estimated_time;

}
<?php
/**
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>

</head>
<body <?php body_class();?>>
    <div class="loader__wrapper">
        <?php if( get_field('loader_logo', 'option') ) { ?>
        <div class="loader__image">
            <img src="<?php the_field('loader_logo', 'option'); ?>" alt="">
        </div>
        <?php } ?>
    </div>
    <?php 
    $header__class = '';
    if( !is_front_page() ) $header__class = 'dark';
    ?>
    <header id="header" class="<?php echo $header__class ; ?>">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="header__logo">
                        <?php the_custom_logo(); ?>
                    </div>
                    <?php wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'main__nav'
                    ) ); ?>
                </div>
            </div>
        </div>
        <div class="menu__btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="mobile__menu">
            <?php wp_nav_menu( array(
                'theme_location'        => 'main',
                'container'             => 'nav',
                'container_class'       => 'main__nav'
            ) ); ?>
        </div>
    </header>
    <main>
<section class="benefits__section grey__section">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="two__line"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2 data-aos="fade-left"><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
        <?php 
        if( have_rows('benefits') ):
            while ( have_rows('benefits') ) : the_row(); 
                $rowType = (int) get_sub_field('choose_type');
                if($rowType == 1){ 
                    $image1 = (get_sub_field('image_1')) ? 'style="background-image: url('.get_sub_field('image_1').');"' : '';
                    $image2 = (get_sub_field('image_2')) ? 'style="background-image: url('.get_sub_field('image_2').');"' : '';
                    ?>
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits image" data-aos="fade-up" data-aos-delay="300">
                                <div class="image__block"<?php echo $image1; ?>></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits text" data-aos="fade-up" data-aos-delay="600">
                                <div class="text-block text-left">
                                    <?php the_sub_field('text'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits image" data-aos="fade-up" data-aos-delay="900">
                                <div class="image__block"<?php echo $image2; ?>></div>
                            </div>
                        </div>
                    </div>
                <?php } else { 
                    $image = (get_sub_field('image')) ? 'style="background-image: url('.get_sub_field('image').');"' : '';
                    ?>
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits text" data-aos="fade-up" data-aos-delay="1200">
                                <div class="text-block text-left">
                                   <?php the_sub_field('text_1'); ?> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits image" data-aos="fade-up" data-aos-delay="1500">
                                <div class="image__block"<?php echo $image; ?>></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="benefits text" data-aos="fade-up" data-aos-delay="1800">
                                <div class="text-block text-left">
                                    <?php the_sub_field('text_2'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php endwhile;
        endif; ?>
    </div>
</section>
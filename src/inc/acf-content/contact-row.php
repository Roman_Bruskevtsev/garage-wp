<?php if( have_rows('contact_column') ): ?>
<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="one__line"></div>
            </div>
        </div>
        <div class="row">
            <?php while ( have_rows('contact_column') ) : the_row(); ?>
            <div class="col-lg-4 text-left">
                <div class="contact__block">
                    <?php the_sub_field('text'); ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?>
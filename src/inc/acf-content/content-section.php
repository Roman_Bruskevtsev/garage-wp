<?php
$image = (get_sub_field('background')) ? 'style="background-image: url('.get_sub_field('background').');"' : '';
?>
<section class="waiting_section padding"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <?php if(get_sub_field('title')) { ?><h2 class="white" data-aos="fade-left"><?php the_sub_field('title'); ?></h2><?php } ?>
                <div class="text-block text-left" data-aos="fade-up" data-aos-delay="300">
                    <?php the_sub_field('text'); ?>
                    <?php if(get_sub_field('link')) { ?>
                    <a href="<?php the_sub_field('link'); ?>" class="btn big transparent"><?php the_sub_field('label'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
        
</section>
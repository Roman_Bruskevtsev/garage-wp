<div class="main__slider">
    <?php if( have_rows('slide') ):
        while ( have_rows('slide') ) : the_row(); 
            $background = (get_sub_field('background')) ? 'style="background-image: url('.get_sub_field('background').');"' : '';
            $icon = (get_sub_field('icon')) ? '<img src="'.get_sub_field('icon').'" alt="Icon">' : '';
            $title = (get_sub_field('title')) ? '<h2>'.get_sub_field('title').'</h2>' : '';
        ?>
        <div class="slide"<?php echo $background; ?>>
            <div class="slide__content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-8">
                            <?php echo $icon; ?>
                            <?php echo $title; ?>
                            <?php the_sub_field('text'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endwhile;
    endif; ?>
</div>
<?php if(get_sub_field('title')){ ?>
<div class="page__banner small">
    <div class="banner__content">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 text-left">
                    <h1 data-aos="fade-left" data-aos-delay="300"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } 
$gallery = get_sub_field('gallery');
if( $gallery ) { ?>
<div class="page__content nopadding">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="gallery__slider">
                    <?php foreach( $gallery as $image ): ?>
                    <div class="slide" style="background-image: url(<?php echo $image['url']?>);"></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
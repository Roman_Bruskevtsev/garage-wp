<div class="map">
    <div class="map-wrapper" id="map-canvas" data-lat="<?php the_sub_field('latitude'); ?>" data-lng="<?php the_sub_field('longitude'); ?>" data-zoom="<?php the_sub_field('zoom'); ?>" data-style="1" data-marker="<?php the_sub_field('marker'); ?>"></div>
    <div class="markers-wrapper addresses-block">
        <a class="marker" data-rel="map-canvas" data-lat="<?php the_sub_field('latitude'); ?>" data-lng="<?php the_sub_field('longitude'); ?>"></a>
    </div>
</div>
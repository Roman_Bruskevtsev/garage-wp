<div class="page__content">
    <div class="container">
        <?php if(get_sub_field('title')){ ?>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="page__title">
                    <h1 data-aos="fade-left"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('benefit') ): ?>
        <div class="row justify-content-md-center">
            <?php while ( have_rows('benefit') ) : the_row(); ?>
                <div class="col-md-12 col-lg-3">
                    <div class="benefit__block" data-aos="fade-up">
                        <?php if(get_sub_field('icon')){ ?>
                        <div class="benefit_image">
                            <img src="<?php the_sub_field('icon'); ?>" alt="">
                        </div>
                        <?php } ?>
                        <div class="benefit__text">
                            <h4><?php the_sub_field('title'); ?></h4>
                            <p><?php the_sub_field('text'); ?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<div class="page__content grey__section">
    <div class="container">
        <?php if(get_sub_field('title')){ ?>
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="page__title">
                    <h1 data-aos="fade-left"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('proposals') ): ?>
        <div class="row justify-content-md-center">
            <div class="col-md-9">
                <div class="row">
                    <?php while ( have_rows('proposals') ) : the_row(); ?>
                    <div class="col-lg-4">
                        <div class="service__table" data-aos="fade-up">
                            <?php if(get_sub_field('title')){ ?>
                            <div class="service__head">
                                <div class="service__cell"><?php the_sub_field('title'); ?></div>
                            </div>
                            <?php } 
                            if( have_rows('price_row') ): ?>
                            <div class="service__body">
                                <div class="service__cell">
                                    <?php while ( have_rows('price_row') ) : the_row(); ?>
                                        <div class="service__row">
                                            <?php the_sub_field('price'); ?>  <span>₴</span> / <?php the_sub_field('time'); ?>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="service__footer"></div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
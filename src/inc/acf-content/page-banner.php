<?php
$image = (get_field('background')) ? 'style="background-image: url('.get_field('background').');"' : '';
?>
<div class="page__banner"<?php echo $image; ?>>
    <div class="banner__content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 text-left">
                    <?php if( get_field('text') ){ ?><h2 data-aos="fade-left" data-aos-delay="300"><?php the_field('text'); ?></h2><?php } ?>
                    <?php if( get_field('subtext') ){ ?><p data-aos="fade-left" data-aos-delay="800"><?php the_field('subtext'); ?></p><?php } ?>
                    <?php if( is_single() ) { ?><span class="post__details" data-aos="fade-left" data-aos-delay="500"><?php echo get_the_date(); ?> / <?php echo garage_reading_time(); ?></span><?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
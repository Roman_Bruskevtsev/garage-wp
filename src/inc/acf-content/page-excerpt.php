<div class="page__excerpt grey__section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <p data-aos="fade-left">
                    <?php the_sub_field('text'); ?>
                </p>
            </div>
        </div>
    </div>
</div>
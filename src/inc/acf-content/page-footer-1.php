<?php
$image = (get_sub_field('background')) ? 'style="background-image: url('.get_sub_field('background').');"' : '';
?>
<div class="page__bottom"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-left">
                <?php if( get_sub_field('title') ){ ?><h3 data-aos="fade-left"><?php the_sub_field('title'); ?></h3><?php } ?>
                <?php if( get_sub_field('text') ){ ?><p data-aos="fade-left" data-aos-delay="300"><?php the_sub_field('text'); ?></p><?php } ?>
            </div>
        </div>
    </div>
</div>
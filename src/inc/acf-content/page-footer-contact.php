<?php
$image = (get_sub_field('background')) ? 'style="background-image: url('.get_sub_field('background').');"' : '';
?>
<div class="page__bottom"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-left">
                <h2 data-aos="fade-left"><?php the_sub_field('title'); ?></h2>
                <p data-aos="fade-left" data-aos-delay="300"><?php the_sub_field('subtitle'); ?></p>
                <div class="contact__form" data-aos="fade-up">
                    <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
                </div>
            </div>
        </div>
    </div>
</div>
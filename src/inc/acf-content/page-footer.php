<?php
$image = (get_field('ft_background')) ? 'style="background-image: url('.get_field('ft_background').');"' : '';
?>
<div class="page__bottom"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-left">
                <?php if( get_field('ft_title') ){ ?><h2 data-aos="fade-left" data-aos-delay="300"><?php the_field('ft_title'); ?></h2><?php } ?>
                <?php if( get_field('ft_text') ){ ?><p data-aos="fade-left" data-aos-delay="800"><?php the_field('ft_text'); ?></p><?php } ?>
                <?php if( get_field('ft_link') ){ ?><a class="btn big transparent" href="<?php the_field('ft_link'); ?>"><?php the_field('ft_link_label'); ?></a><?php } ?>
            </div>
        </div>
    </div>
</div>
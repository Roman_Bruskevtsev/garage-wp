<div class="page__content grey__section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 text-left">
                <div class="page__title">
                    <h1 data-aos="fade-left"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-3 offset-lg-2">
                <div class="table__price" data-aos="fade-up">
                    <div class="table__header">
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_title_1'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="table__body">
                        <div class="table__row">
                            <div class="table__cell__label"><?php the_sub_field('row_label_1'); ?></div>
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_1_cell_1'); ?> <span>₴</span></div>
                            </div>
                        </div>
                        <div class="table__row">
                            <div class="table__cell__label"><?php the_sub_field('row_label_2'); ?></div>
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_1_cell_2'); ?> <span>₴</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="table__price" data-aos="fade-up">
                    <div class="table__header">
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><i></i><i></i> <?php the_sub_field('table_title_2'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="table__body">
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_2_cell_1'); ?> <span>₴</span></div>
                            </div>
                        </div>
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_2_cell_2'); ?> <span>₴</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="table__price" data-aos="fade-up">
                    <div class="table__header">
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><i></i><i></i><i></i> <?php the_sub_field('table_title_3'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="table__body">
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_3_cell_1'); ?> <span>₴</span></div>
                            </div>
                        </div>
                        <div class="table__row">
                            <div class="table__cell">
                                <div class="cell__content"><?php the_sub_field('table_3_cell_2'); ?> <span>₴</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="about__section">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="one__line"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2 data-aos="fade-left"><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
        <div class="row">
            <?php
            $chosen_posts = get_sub_field('choose_posts');
            $posts_args = array(
                'posts_per_page'    => 3,
                'post__in'          => $chosen_posts,
                'orderby'           => 'post__in'
            );

            $query = new WP_Query( $posts_args );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post(); 
                    $post_thumb = (get_the_post_thumbnail( get_the_ID() )) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'about-post-size' ).')"' : '';
                    ?>
                    <div class="col-md-12 col-lg-4">
                        <div class="post__item hover__block" data-aos="fade-up" data-aos-delay="300">
                            <div class="post__thumb"<?php echo $post_thumb; ?>></div>
                            <div class="post__info">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="green-btn big">Більше</a>
                            </div>
                        </div>
                    </div>
                <?php }
            } else {
                
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>
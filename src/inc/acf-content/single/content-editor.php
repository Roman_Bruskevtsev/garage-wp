<div class="row justify-content-center">
    <div class="col-lg-6" data-aos="fade-up">
        <?php the_sub_field('content_editor'); ?>
    </div>
</div>
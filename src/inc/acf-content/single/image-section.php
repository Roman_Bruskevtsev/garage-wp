<div class="row justify-content-center">
    <div class="col-lg-8" data-aos="fade-up">
        <figure>
            <?php if( get_sub_field('image') ) { ?><p><img src="<?php the_sub_field('image'); ?>" alt=""></p><?php } ?>
            <figcaption><?php the_sub_field('text'); ?></figcaption>
        </figure>
    </div>
</div>
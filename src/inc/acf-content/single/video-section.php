
    <div class="col-lg-8" data-aos="fade-up">
        <?php if( get_sub_field('video_code') ) { ?><figure>
            <p><iframe width="830" height="480" src="https://www.youtube.com/embed/<?php the_sub_field('video_code'); ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe></p>
            <?php if( get_sub_field('text') ) { ?><figcaption><?php the_sub_field('text'); ?></figcaption><?php } ?>
        </figure>
        <?php } ?>
    </div>

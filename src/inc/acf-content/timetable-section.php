<div class="page__content padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="two__line top"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 offset-lg-1 text-left">
                <div class="page__title">
                    <h1 data-aos="fade-left"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9 offset-lg-2">
                <div class="table__timetable" data-aos="fade-up">
                    <div class="table__body">
                        <div class="table__column">
                            <div class="table__cell"></div>
                            <div class="table__cell">07:00</div>
                            <div class="table__cell">08:00</div>
                            <div class="table__cell">09:00</div>
                            <div class="table__cell">10:00</div>
                            <div class="table__cell">11:00</div>
                            <div class="table__cell">12:00</div>
                            <div class="table__cell">13:00</div>
                            <div class="table__cell">14:00</div>
                            <div class="table__cell">15:00</div>
                            <div class="table__cell">16:00</div>
                            <div class="table__cell">17:00</div>
                            <div class="table__cell">18:00</div>
                            <div class="table__cell">19:00</div>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">ПН</div>
                            <?php 
                                if( have_rows('monday_timetable') ):
                                    while ( have_rows('monday_timetable') ) : the_row(); 
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">ВТ</div>
                            <?php 
                                if( have_rows('tuesday_timetable') ):
                                    while ( have_rows('tuesday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">СР</div>
                            <?php 
                                if( have_rows('wednesday_timetable') ):
                                    while ( have_rows('wednesday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">ЧТ</div>
                            <?php 
                                if( have_rows('thursday_timetable') ):
                                    while ( have_rows('thursday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">ПТ</div>
                            <?php 
                                if( have_rows('friday_timetable') ):
                                    while ( have_rows('friday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">СБ</div>
                            <?php 
                                if( have_rows('saturday_timetable') ):
                                    while ( have_rows('saturday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                        <div class="table__column">
                            <div class="table__cell">НД</div>
                            <?php 
                                if( have_rows('sunday_timetable') ):
                                    while ( have_rows('sunday_timetable') ) : the_row();  
                                    $trainer = (get_sub_field('choose_trainer')) ? '<a href="'.get_the_permalink(get_sub_field('choose_trainer')).'">'.get_the_title(get_sub_field('choose_trainer')).'</a>' : '';
                                    ?>
                                    <div class="table__cell"><div class="cell__content"><?php echo $trainer; ?></div></div>
                                <?php endwhile;
                            endif; ?>
                        </div>
                    </div>
                    <div class="table__footer">
                        <div class="table__row">
                            <div class="table__cell">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="one__line"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 offset-lg-1 text-left">
                <div class="page__title">
                    <h1 data-aos="fade-left"><?php the_sub_field('title'); ?></h1>
                </div>
            </div>
        </div>
        <?php
        $posts_id = get_sub_field('choose_trainers');
        $posts_args = array(
            'posts_per_page'    => -1,
            'post__in'          => $posts_id,
            'orderby'           => 'post__in',
            'post_type'         => 'trainers'
        );

        $query = new WP_Query( $posts_args );

        if ( $query->have_posts() ) { ?>
        <div class="row justify-content-md-center">
            <?php while ( $query->have_posts() ) { $query->the_post(); 
                $post_thumb = (get_the_post_thumbnail( get_the_ID() )) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'large' ).')"' : '';
                ?>
            <div class="col-lg-3">
                <div class="trainer__block hover__block" data-aos="fade-up" data-aos-delay="300">
                    <div class="thumb"<?php echo $post_thumb; ?>></div>
                    <div class="info__block text-left">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="green-btn small"><?php echo MORE; ?></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php }
        wp_reset_postdata(); ?>
        <div class="row">
            <div class="col">
                <div class="two__line bottom"></div>
            </div>
        </div>
    </div>
</div>
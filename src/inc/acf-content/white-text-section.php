<div class="page__bottom nobackground padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-left">
                <?php if(get_sub_field('title')){ ?><h2 data-aos="fade-left"><?php the_sub_field('title'); ?></h2><?php } ?>
                <?php if(get_sub_field('text')){ ?><p data-aos="fade-left" data-aos-delay="300"><?php the_sub_field('text'); ?></p><?php } ?>
                <?php if(get_sub_field('link')){ ?><a class="btn big transparent" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_label'); ?></a><?php } ?>
            </div>
        </div>
    </div>
</div>
<?php
add_action('init', 'garage_trainers_post', 0);

function garage_trainers_post() {

  //Register new post type
  $products_labels = array(
  'name'                => __('Trainers', 'garage'),
  'add_new'             => __('Add Trainer', 'garage')
  );

  $products_args = array(
  'label'               => __('Trainers', 'garage'),
  'description'         => __('Trainers information page', 'garage'),
  'labels'              => $products_labels,
  'supports'            => array( 'title', 'thumbnail', 'editor', 'excerpt'),
  'taxonomies'          => array( 'trainers-categories' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-universal-access'
  );
  register_post_type( 'trainers', $products_args );
}

<?php
define("MORE",               "Більше");
define("AGE",                "ВІК:");
define("WEIGHT",             "ВАГА:");
define("HEIGHT",             "РІСТ:");
define("SPECIALIZATION",     "СПЕЦІАЛІЗАЦІЯ:");
define("NEXTPAGE",           "Наступна");
define("PREVPAGE",           "Попередня");
define("POSTS",              "Публікацій");
define("RELATEDPOSTS",       "Вам також може бути<br>цікаво:");
define("MOREPUBLICATION",    "Більше публікацій");
define("SHARING",            "Сподобалась стаття?  Поділись з друзями!");
define("MINUTE",             " хвилинa");
define("MINUTES",            " хвилин");
define("SECONDS",            " секунд");
define("FORREADING",         " для прочитання");

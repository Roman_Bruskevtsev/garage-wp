<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

get_header(); 

$image = (get_field('blog_header_background', 'option')) ? 'style="background-image: url('.get_field('blog_header_background', 'option').');"' : '';

?>
<div class="page__banner"<?php echo $image; ?>></div>
<?php if ( have_posts() ) : ?>
    <div class="page__content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="one__line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1 text-left">
                    <div class="page__title">
                        <h1 data-aos="fade-left"><?php the_field('blog_title', 'option'); ?></h1>
                        <p data-aos="fade-left">
                            <?php the_field('blog_text', 'option'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-lg-10 offset-lg-1 text-left">
                <div class="blog__navigation" data-aos="fade-left">
                    <span class="posts__count"><?php echo wp_count_posts()->publish; ?> <?php echo POSTS; ?></span>
                    <ul class="navigation__block">
                        <?php global $wp_query;
                        $big = 99999999;
                        echo paginate_links(
                            array(
                                'base'              => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
                                'format'            =>'?paged=%#%',
                                'current'           =>max(1,get_query_var('paged')),
                                'total'             =>$wp_query->max_num_pages,
                                'show_all'          => true,
                                'prev_text'         =>'<span>'.PREVPAGE.'</span>',
                                'next_text'         =>'<span>'.NEXTPAGE.'</span>',
                                'before_page_number'=>'<span>',
                                'after_page_number' => '</span>',
                                'type'              => 'list'
                            )
                        ); ?>
                    </ul>
                </div>
                <div class="blog__wrapper">
                    <div class="row">
                        <?php while ( have_posts() ) : the_post();

                            get_template_part( 'template-parts/post/content', get_post_format() );

                        endwhile; ?>
                    </div>
                </div>
                <div class="blog__navigation" data-aos="fade-left">
                        <span class="posts__count"><?php echo wp_count_posts()->publish; ?> <?php echo POSTS; ?></span>
                        <ul class="navigation__block">
                        <?php global $wp_query;
                        $big = 99999999;
                        echo paginate_links(
                            array(
                                'base'              => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
                                'format'            =>'?paged=%#%',
                                'current'           =>max(1,get_query_var('paged')),
                                'total'             =>$wp_query->max_num_pages,
                                'show_all'          => true,
                                'prev_text'         =>'<span>'.PREVPAGE.'</span>',
                                'next_text'         =>'<span>'.NEXTPAGE.'</span>',
                                'before_page_number'=>'<span>',
                                'after_page_number' => '</span>',
                                'type'              => 'list'
                            )
                        ); ?>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php else :

        echo '
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="no__content">
                            <h1>'.__('Nothing to show', 'garage').'</h1>
                        </div>
                    </div>
                </div>
            </div>
        ';

    endif; ?>
    
<?php get_footer();
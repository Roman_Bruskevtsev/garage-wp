<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

get_header();

if(get_field('show_page_banner')) get_template_part( 'inc/acf-content/page-banner' );

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'content_editor' ):
            the_sub_field('text');
        elseif( get_row_layout() == 'full_height_slider' ): 
            get_template_part( 'inc/acf-content/full-height-slider' );
        elseif( get_row_layout() == 'posts_section' ): 
            get_template_part( 'inc/acf-content/posts-section' );
        elseif( get_row_layout() == 'benefits_section' ): 
            get_template_part( 'inc/acf-content/benefits-section' );
        elseif( get_row_layout() == 'content_section' ): 
            get_template_part( 'inc/acf-content/content-section' );
        elseif( get_row_layout() == 'subscription_table' ): 
            get_template_part( 'inc/acf-content/subscription-section' );
        elseif( get_row_layout() == 'personal_training_price' ): 
            get_template_part( 'inc/acf-content/personal-training' );
        elseif( get_row_layout() == 'trainers_section' ): 
            get_template_part( 'inc/acf-content/trainers-section' );
        elseif( get_row_layout() == 'page_footer_1' ): 
            get_template_part( 'inc/acf-content/page-footer-1' );
        elseif( get_row_layout() == 'timetable' ): 
            get_template_part( 'inc/acf-content/timetable-section' );
        elseif( get_row_layout() == 'page_footer_contact' ): 
            get_template_part( 'inc/acf-content/page-footer-contact' );
        elseif( get_row_layout() == 'page_excerpt' ): 
            get_template_part( 'inc/acf-content/page-excerpt' );
        elseif( get_row_layout() == 'massage_benefits' ): 
            get_template_part( 'inc/acf-content/massage-benefits' );
        elseif( get_row_layout() == 'massage_proposals' ): 
            get_template_part( 'inc/acf-content/massage-proposals' );
        elseif( get_row_layout() == 'gallery_section' ): 
            get_template_part( 'inc/acf-content/gallery-section' );
        elseif( get_row_layout() == 'white_text_section' ): 
            get_template_part( 'inc/acf-content/white-text-section' );
        elseif( get_row_layout() == 'contact_row' ): 
            get_template_part( 'inc/acf-content/contact-row' );
        elseif( get_row_layout() == 'google_map' ): 
            get_template_part( 'inc/acf-content/google-map' );
        endif;
    endwhile;
else :
    echo '
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="no__content">
                        <h1>'.__('Nothing to show', 'garage').'</h1>
                    </div>
                </div>
            </div>
        </div>
    ';
endif;

get_footer();
<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

get_header();

if(get_field('show_page_banner')) get_template_part( 'inc/acf-content/page-banner' );

$post_thumb = (get_the_post_thumbnail( get_the_ID() )) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'large' ).')"' : '';
?>

<div class="page__content padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="one__line"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 offset-lg-2">
                <div class="trainer__details" data-aos="fade-up" data-aos-delay="300">
                    <div class="thumb"<?php echo $post_thumb; ?>></div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="info__block text-left" data-aos="fade-left" data-aos-delay="600">
                    <ul>
                        <li><?php echo AGE; ?> <?php the_field('age'); ?></li>
                        <li><?php echo WEIGHT; ?> <?php the_field('weight'); ?></li>
                        <li><?php echo HEIGHT; ?> <?php the_field('height'); ?></li>
                        <li><?php echo SPECIALIZATION; ?></li>
                    </ul>
                    <p><?php the_field('specialization'); ?></p>
                    <?php if(get_field('additional_information')) { ?>
                        <p class="title"><b><?php the_field('additional_informations'); ?></b></p>
                    <?php } ?>
                    <ul class="social__links">
                        <?php if(get_field('facebook_link')) { ?><li><a href="<?php the_field('facebook_link'); ?>" class="fb" target="_blank"></a></li><?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 offset-lg-2 text-left">
                <div class="trainer__info text-left" data-aos="fade-up" data-aos-delay="600">
                    <?php while ( have_posts() ) : the_post();
                        the_content(); 
                    endwhile; ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="two__line bottom"></div>
            </div>
        </div>
    </div>
</div>

<?php 
if(get_field('show_page_footer')) get_template_part( 'inc/acf-content/page-footer' );
get_footer();
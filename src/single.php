<?php
/**
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

get_header(); 

if(get_field('show_page_banner')) get_template_part( 'inc/acf-content/page-banner' ); ?>

<div class="page__content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="one__line"></div>
            </div>
        </div>
        <div class="post__content row justify-content-md-center">
            <div class="post__share">
                <ul>
                    <li>
                        <span class="google st-custom-button icon-gplus" data-network="googleplus" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                    <li>
                        <span class="twitter st-custom-button icon-twitter" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                    <li>
                        <span class="facebook st-custom-button icon-facebook" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>" ></span>
                    </li>
                </ul>
            </div>
            <?php
            if( have_rows('content') ):
                while ( have_rows('content') ) : the_row();
                    if( get_row_layout() == 'content_editor' ):
                        get_template_part( 'inc/acf-content/single/content-editor' );
                    elseif( get_row_layout() == 'image_section' ): 
                        get_template_part( 'inc/acf-content/single/image-section' );
                    elseif( get_row_layout() == 'video_section' ): 
                        get_template_part( 'inc/acf-content/single/video-section' );
                    endif;
                endwhile;
            else :
                echo '
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="no__content">
                                    <h1>'.__('Nothing to show', 'garage').'</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            endif; ?>
            <div class="col-lg-6" data-aos="fade-up">
                <hr>
            </div>
        </div>
        <div class="row justify-content-md-center" data-aos="fade-up">
            <div class="col-lg-6">
                <div class="post__share">
                    <h5><?php echo SHARING; ?></h5>
                    <ul>
                        <li>
                        <span class="google st-custom-button icon-gplus" data-network="googleplus" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                    <li>
                        <span class="twitter st-custom-button icon-twitter" data-network="twitter" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></span>
                    </li>
                    <li>
                        <span class="facebook st-custom-button icon-facebook" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>" ></span>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="two__line bottom"></div>
            </div>
        </div>
    </div>
</div>

<?php 

get_template_part( 'template-parts/post/related-post' );

get_footer();
<?php
/**
 * Template part for displaying posts
 *
 * @package WordPress
 * @subpackage Garage
 * @since 1.0
 * @version 1.0
 */

$post_thumb = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'article-thumbnail' ).');"' : '';
?>
<div class="col-lg-6">
    <a class="post__item hover__block" href="<?php the_permalink(); ?>" data-aos="fade-up">
        <div class="post__thumb"<?php echo $post_thumb; ?>></div>
        <div class="post__info">
            <span><?php echo get_the_date(); ?> / <?php echo garage_reading_time(); ?></span>
            <h6><?php the_title(); ?></h6>
            <p>
                <?php the_excerpt(); ?>
            </p>
        </div>
    </a>
</div>

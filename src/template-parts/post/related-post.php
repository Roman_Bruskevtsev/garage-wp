<div class="related__post">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-8">
                <h4 data-aos="fade-left"><?php echo RELATEDPOSTS; ?></h4>
            </div>
        </div>
        <?php
        $post_id = get_the_ID();

        $args = array(
            'posts_per_page'    => 2,
            'orderby'           => 'rand',
            'post__not_in'      => array($post_id)
        );

        $query = new WP_Query( $args );

        if ( $query->have_posts() ) { ?>
        <div class="related__wrapper">
            <div class="row justify-content-md-center">
            <?php while ( $query->have_posts() ) {
                $query->the_post(); 
                $post_thumb = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'article-thumbnail' ).');"' : ''; ?>
                <div class="col-lg-5">
                    <a class="post__item" href="<?php the_permalink(); ?>" data-aos="fade-up">
                        <div class="post__thumb"<?php echo $post_thumb; ?>></div>
                        <div class="post__info">
                            <span><?php echo get_the_date(); ?> / <?php echo garage_reading_time(); ?></span>
                            <h6><?php the_title(); ?></h6>
                            <p>
                                <?php the_excerpt(); ?>
                            </p>
                        </div>
                    </a>
                </div>
            <?php } ?>
            </div>
            <?php 
            $blog_id = get_option('page_for_posts');
            if($blog_id) { ?>
            <div class="row justify-content-md-center text-right">
                <div class="col-lg-10">
                    <a href="<?php echo get_permalink($blog_id); ?>" class="more__posts" data-aos="fade-right"><?php echo MOREPUBLICATION; ?></a>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } 
        wp_reset_postdata();
        ?>
    </div>
</div>